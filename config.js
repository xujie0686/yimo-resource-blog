// 应用全局配置
module.exports = {
  baseUrl: 'http://127.0.0.1:8080',
  //应用信息
  appInfo: {
    // 应用名称
    name: "依沫博客",
    // 应用版本
    version: "1.1.0",
    // 应用logo
    logo: "/static/logo.png",
    // 官方网站
    site_url: "http://yimo.vip"
  }
}
