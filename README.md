<p align="center">
	<img alt="logo" src="http://imgs.emoboy.vip/app-show/log.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">依沫博客资源</h1>
<h4 align="center">基于UniApp开发</h4>
<p align="center">
	<img src="https://img.shields.io/github/license/mashape/apistatus.svg">
</p>

## 平台简介

依沫博客资源变现专为内容/资源下载场景设计，一站式解决内容带货、流量、资源变现，资源博客文章分享难题，目前以实现个人博客，文章发表，资源收集下载，手机壁纸下载，电脑壁纸下载，头像下载，支持抖音，快手，小红书等 100+ 平台的图片视频水印去除，这些内容展示均已实现微信小程序流量主广告变现功能，可根据自己需要进行配置，
另外还加入了积分系统以及积分任务和积分商店,可让用户完成相应的任务获得积分换取奖励，源码无保留可二次开发。

* 应用框架基于[uniapp](https://uniapp.dcloud.net.cn/)，支持小程序、H5、Android和IOS。（流量主流量变现广告目前只支持微信小程序）
* 前端组件采用[uni-ui](https://github.com/dcloudio/uni-ui)，全端兼容的高性能UI框架。
* 阿里云免费试用3个月，新老用户同享 99/年：[点我进入](https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=ua4mkdxk)
* 需要后端代码请打开微信搜索公众号关注 “依沫资源网” 或微信扫面下方二维码关注微信公众号发送 "依沫博客后端源码" 即可获取

<img src="http://imgs.emoboy.vip/app-show/gongzhonghao.png" alt="公众号" width = 40%/>

## 技术文档

- 运行之前请安装uview-ui组件依赖，可打开项目根目录运行 cmd 输入 npm install uview-ui
- 如需微信小程序运行或实现微信流量主小程序流量变现请修改 manifest.json 微信小程序的AppID，以及在后台管理端的App配置项配置自己的广告id
- 问题联系作者QQ： 1149297946 或 微信：yimoziyuan666
- 打开微信扫描下面的小程序码即可体验小程序

<img src="http://imgs.emoboy.vip/app-show/miniapp.jpg" alt="小程序演示"/>


## 小程序部分演示图

<table>
    <tr>
        <td><img src="http://imgs.emoboy.vip/app-show/%E9%A6%96%E9%A1%B5.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E6%90%9C%E7%B4%A2.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E6%88%91%E7%9A%84.png"/></td>
    </tr>
    <tr>
        <td><img src="http://imgs.emoboy.vip/app-show/%E6%96%87%E7%AB%A0%E8%AF%A6%E6%83%85.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E6%96%87%E7%AB%A0%E8%AF%A6%E6%83%852.png"/></td>
		<td><img src="http://imgs.emoboy.vip/app-show/%E4%B8%8B%E8%BD%BD.png"/></td>
    </tr>
    <tr>
        <td><img src="http://imgs.emoboy.vip/app-show/%E7%A7%AF%E5%88%86.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E6%B0%B4%E5%8D%B01.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E5%A3%81%E7%BA%B8%E8%AF%A6%E6%83%85.png"/></td>
    </tr>
	<tr>
        <td><img src="http://imgs.emoboy.vip/app-show/%E6%89%8B%E6%9C%BA%E5%A3%81%E7%BA%B8.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E5%A4%B4%E5%83%8F.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E7%94%B5%E8%84%91%E5%A3%81%E7%BA%B8.png"/></td>
    </tr>
</table>

## 管理后台部分演示图

<table>
    <tr>
        <td><img src="http://imgs.emoboy.vip/app-show/%E5%90%8E%E5%8F%B01.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E5%90%8E%E5%8F%B02.png"/></td>
    </tr>
    <tr>
        <td><img src="http://imgs.emoboy.vip/app-show/%E5%90%8E%E5%8F%B03.png"/></td>
        <td><img src="http://imgs.emoboy.vip/app-show/%E5%90%8E%E5%8F%B04.png"/></td>
    </tr>
</table>

* 若您喜欢对您有帮助的话，麻烦客官动动发财的手给小二一个 Star ，您的支持是我们不断进步的源泉！！！

